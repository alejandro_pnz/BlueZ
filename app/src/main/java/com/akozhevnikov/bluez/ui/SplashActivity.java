package com.akozhevnikov.bluez.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.akozhevnikov.bluez.R;
import com.akozhevnikov.bluez.utils.Settings;

public class SplashActivity extends AppCompatActivity {
    private Handler handler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        handler = new Handler();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(500);
                    handler.post(new Runnable() {
                        public void run() {
                            goToNextScreen();
                        }
                    });
                } catch (InterruptedException e) {
                }
            }
        };

        thread.start();
    }

    protected void goToNextScreen() {
        if (Settings.getUserName().isEmpty()) {
            Intent loginIntent = new Intent(this, LoginActivity.class);
            startActivity(loginIntent);
            finish();
        } else {
            Intent intent = new Intent(this, ChatListActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
