package com.akozhevnikov.bluez.ui;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.akozhevnikov.bluez.R;
import com.akozhevnikov.bluez.model.Message;
import com.akozhevnikov.bluez.model.User;
import com.akozhevnikov.bluez.utils.Settings;
import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MessageHolder extends RecyclerView.ViewHolder {
    private static final String TIME_PATTERN = "hh:mm";
    private static final SimpleDateFormat DATE_FORMAT;

    static {
        DATE_FORMAT = new SimpleDateFormat(TIME_PATTERN, java.util.Locale.getDefault());
    }

    private TextView sender;
    private TextView messageText;
    private TextView sendedTime;
    private ImageView senderAvatar;
    private CardView messageCardView;
    private boolean isConversation;
    private int color;

    public MessageHolder(View itemView, boolean isConversation) {
        super(itemView);

        sender = (TextView) itemView.findViewById(R.id.sender_name);
        messageText = (TextView) itemView.findViewById(R.id.sended_message);
        sendedTime = (TextView) itemView.findViewById(R.id.send_time);
        senderAvatar = (ImageView) itemView.findViewById(R.id.sender_avatar);
        messageCardView = (CardView) itemView.findViewById(R.id.message_card);

        this.isConversation = isConversation;

        ColorGenerator generator = ColorGenerator.MATERIAL;
        color = generator.getRandomColor();
    }

    public void init(Message message) {
        sendedTime.setText(DATE_FORMAT.format(new Date(message.getCreatedAt())));
        messageText.setText(message.getText());

        User messageSender = message.getSender();
        StringBuilder name = new StringBuilder();
        name.append(messageSender.getFirstName());
        if (messageSender.getLastName() != null) {
            name.append(' ');
            name.append(messageSender.getLastName());
        }

        sender.setText(name);

        TextDrawable drawable = TextDrawable.builder()
                .buildRound(message
                        .getSender()
                        .getUsername()
                        .substring(0, 1)
                        .toUpperCase(), color);

        senderAvatar.setImageDrawable(drawable);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        if (message.getSender().getUsername().equals(Settings.getUserName())) {
            params.setMargins(72, 0, 16, 8);
        } else {
            params.setMargins(16, 0, 72, 8);
            if (isConversation) {
                senderAvatar.setVisibility(View.VISIBLE);
                sender.setVisibility(View.VISIBLE);
            }
        }
        messageCardView.setLayoutParams(params);
    }
}
