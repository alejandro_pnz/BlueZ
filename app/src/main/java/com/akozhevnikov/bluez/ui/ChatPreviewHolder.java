package com.akozhevnikov.bluez.ui;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.akozhevnikov.bluez.R;
import com.akozhevnikov.bluez.model.Chat;
import com.akozhevnikov.bluez.model.Message;
import com.akozhevnikov.bluez.model.User;
import com.akozhevnikov.bluez.utils.MessageHelper;
import com.akozhevnikov.bluez.utils.Settings;
import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ChatPreviewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private static final String TIME_PATTERN = "hh:mm";
    private static final SimpleDateFormat DATE_FORMAT;

    static {
        DATE_FORMAT = new SimpleDateFormat(TIME_PATTERN, java.util.Locale.getDefault());
    }

    @Bind(R.id.dialog_avatar)
    ImageView avatar;
    @Bind(R.id.dialog_name)
    TextView chatName;
    @Bind(R.id.dialog_message)
    TextView chatMessage;
    @Bind(R.id.dialog_group)
    ImageView dialogGroupAvatar;
    @Bind(R.id.dialog_last_message_time)
    TextView lastMessageTime;
    @Bind(R.id.dialog_messenger)
    TextView lastMessenger;

    private Chat chat;
    private Context context;
    private int color;

    public ChatPreviewHolder(Context context, View itemView) {
        super(itemView);

        ButterKnife.bind(this, itemView);

        this.context = context;

        //TODO Add time
        itemView.setOnClickListener(this);

        ColorGenerator generator = ColorGenerator.MATERIAL;
        color = generator.getRandomColor();
    }

    public void init(Chat chat) {
        this.chat = chat;

        TextDrawable drawable = null;

        if (chat.getUsers().size() == 2) {
            for (User user : chat.getUsers()) {
                if (!user.getUsername().equals(Settings.getUserName())) {
                    StringBuilder name = new StringBuilder();
                    name.append(user.getFirstName());
                    if (user.getLastName() != null) {
                        name.append(' ');
                        name.append(user.getLastName());
                    }
                    chatName.setText(name);
                    drawable = TextDrawable.builder()
                            .buildRound(user.getFirstName().substring(0, 1), color);
                }
            }
        } else {
            chatName.setText(chat.getName());
            dialogGroupAvatar.setVisibility(View.VISIBLE);

            drawable = TextDrawable.builder()
                    .buildRound(chat.getName().substring(0, 1), color);
        }

        List<Message> messages = MessageHelper.getMessagesByChat(chat);
        int messagesCount = messages.size();
        if (messagesCount > 0) {
            Message lastMessage = messages.get(messagesCount - 1);
            chatMessage.setText(lastMessage.getText());
            lastMessageTime.setText(DATE_FORMAT.format(new Date(lastMessage.getCreatedAt())));
            if(!lastMessage.getSender().getUsername().equals(Settings.getUserName())){
                lastMessenger.setVisibility(View.VISIBLE);
                lastMessenger.setText(lastMessage.getSender().getFirstName());
            }
        }

        avatar.setImageDrawable(drawable);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtra(ChatActivity.CHAT_ID, chat.getId());
        context.startActivity(intent);
    }
}
