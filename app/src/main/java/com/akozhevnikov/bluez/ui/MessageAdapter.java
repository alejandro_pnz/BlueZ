package com.akozhevnikov.bluez.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.akozhevnikov.bluez.R;
import com.akozhevnikov.bluez.model.Chat;
import com.akozhevnikov.bluez.model.Message;
import com.akozhevnikov.bluez.utils.MessageHelper;

import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageHolder> {
    private List<Message> messages;
    private Chat chat;

    public MessageAdapter(Chat chat) {
        this.chat = chat;
        update();
    }

    @Override
    public MessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_holder, parent, false);
        MessageHolder holder;
        if (chat.getUsers().size() > 2) {
            holder = new MessageHolder(view, true);
        } else {
            holder = new MessageHolder(view, false);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(MessageHolder holder, int position) {
        holder.init(messages.get(position));
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public void update() {
        messages = MessageHelper.getMessagesByChat(chat);
        notifyDataSetChanged();
    }
}
