package com.akozhevnikov.bluez.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.akozhevnikov.bluez.R;
import com.akozhevnikov.bluez.model.Chat;
import com.akozhevnikov.bluez.model.User;
import com.akozhevnikov.bluez.utils.MessageHelper;
import com.akozhevnikov.bluez.utils.Settings;

import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class ChatActivity extends AppCompatActivity {
    public static final String CHAT_ID = "CHAT_ID";

    @Bind(R.id.chat_send)
    ImageView sendMessageButton;

    @Bind(R.id.chat_attach)
    ImageView addAttachmentsButton;

    @Bind(R.id.chat_smile)
    ImageView showSmilesButton;

    @Bind(R.id.chat_message_text)
    EditText messageEditText;

    @Bind(R.id.message_list)
    RecyclerView messageList;

    private Chat chat;
    private User currentUser;
    private MessageAdapter messageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);

        String currentUserName = Settings.getUserName();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        View titleActionBarView = getLayoutInflater().inflate(R.layout.chat_title_view, null);
        TextView titleView = (TextView) titleActionBarView.findViewById(R.id.chat_title);
        TextView infoView = (TextView) titleActionBarView.findViewById(R.id.chat_info);
        toolbar.addView(titleActionBarView);

        Bundle extra = getIntent().getExtras();
        int chatId = extra.getInt(CHAT_ID);
        chat = MessageHelper.getChatById(chatId);
        if (chat != null) {
            if (chat.getUsers().size() > 2) {
                titleView.setText(chat.getName());
                infoView.setText(getString(R.string.title_info_members, chat.getUsers().size()));
            } else {
                for (User user : chat.getUsers()) {
                    if (!user.getUsername().equals(currentUserName)) {
                        titleView.setText(user.getFirstName());
                        //TODO save in DB
                        String lastTime = "11:40 PM";
                        infoView.setText(getString(R.string.title_info_last_seen, lastTime));
                        break;
                    }
                }
            }
        } else {
            Toast.makeText(this, R.string.error_due_operation, Toast.LENGTH_SHORT).show();
            finish();
        }

        currentUser = MessageHelper.getUsersWhere(User.USERNAME, currentUserName).get(0);

        messageAdapter = new MessageAdapter(chat);
        messageList.setLayoutManager(new LinearLayoutManager(this));
        messageList.setAdapter(messageAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.chat_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menu_chat_clear:
                break;
            case R.id.menu_chat_delete:
                break;
            case R.id.menu_chat_search:
                break;
        }
        return true;
    }

    @SuppressWarnings("unused")
    @OnClick(R.id.chat_send)
    void sendMessage() {
        if (!TextUtils.isEmpty(messageEditText.getText().toString())) {
            //through the network

            //and then into the database
            MessageHelper.createMessage(messageEditText.getText().toString(),
                    Calendar.getInstance().getTimeInMillis(), currentUser, chat.getId());
            messageEditText.getText().clear();
            messageAdapter.update();
        }
    }

    @SuppressWarnings("unused")
    @OnTextChanged(R.id.chat_message_text)
    void onTextChanged(CharSequence text) {
        if (!TextUtils.isEmpty(text)) {
            sendMessageButton.setVisibility(View.VISIBLE);
            addAttachmentsButton.setVisibility(View.GONE);
        } else {
            sendMessageButton.setVisibility(View.GONE);
            addAttachmentsButton.setVisibility(View.VISIBLE);
        }
    }
}
