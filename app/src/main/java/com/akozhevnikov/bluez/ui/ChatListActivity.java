package com.akozhevnikov.bluez.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.akozhevnikov.bluez.R;
import com.akozhevnikov.bluez.model.Chat;
import com.akozhevnikov.bluez.model.User;
import com.akozhevnikov.bluez.utils.MessageHelper;
import com.akozhevnikov.bluez.utils.Settings;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.RealmList;

public class ChatListActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        SharedPreferences.OnSharedPreferenceChangeListener {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.drawer_layout)
    DrawerLayout drawer;
    @Bind(R.id.nav_view)
    NavigationView navigationView;
    @Bind(R.id.chat_list)
    RecyclerView chatList;
    private TextView loginTextView;
    private TextView nameTextView;
    private ImageView avatarImageView;

    private User currentUser;
    private ChatListAdapter chatListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_chat_list);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        View headerLayout = navigationView.inflateHeaderView(R.layout.nav_header_chat);
        nameTextView = (TextView) headerLayout.findViewById(R.id.account_name);
        loginTextView = (TextView) headerLayout.findViewById(R.id.account_login);
        avatarImageView = (ImageView) headerLayout.findViewById(R.id.account_avatar);

        String login = Settings.getUserName();
        loginTextView.setText(login);

        List<User> users =
                MessageHelper.getUsersWhere(User.USERNAME, login);

        if (users.size() > 0) {
            currentUser = users.get(0);

            StringBuilder name = new StringBuilder();
            name.append(currentUser.getFirstName());
            if (currentUser.getLastName() != null) {
                name.append(' ');
                name.append(currentUser.getLastName());
            }
            nameTextView.setText(name);
        } else {
            currentUser = MessageHelper.createUser("fenix", "Marcus", "Fenix", null);
            User cole = MessageHelper.createUser("cole", "Augustus", "Cole", null);
            User baird = MessageHelper.createUser("baird", "Damon", "Baird", null);
            User dom = MessageHelper.createUser("dom", "Dominic", "Santiago", null);
            RealmList<User> chatUsers = new RealmList<>(currentUser, cole, baird, dom);
            Chat domChat = MessageHelper.createChat(new RealmList<>(currentUser, dom), null, null);
            MessageHelper.createChat(new RealmList<>(currentUser, cole), null, null);
            MessageHelper.createChat(new RealmList<>(currentUser, baird), null, null);
            Chat deltaChat = MessageHelper.createChat(chatUsers, currentUser, "Delta Squad");

            MessageHelper.createMessage("Looks like you need an access code.", System.currentTimeMillis(), currentUser,
                    domChat.getId());
            MessageHelper.createMessage("Hmm... Got one?", System.currentTimeMillis(), currentUser,
                    domChat.getId());
            MessageHelper.createMessage("Yeah, in my other pants.", System.currentTimeMillis(), dom,
                    domChat.getId());

            MessageHelper.createMessage("Cole, what's you're status?", System.currentTimeMillis(), currentUser,
                    deltaChat.getId());
            MessageHelper.createMessage("Shit yeah, baby we got the hook-up! They given us a big-ass dinner!",
                    System.currentTimeMillis(), cole, deltaChat.getId());
            MessageHelper.createMessage("We are gonna get dysentery from this shit.", System.currentTimeMillis(),
                    baird, deltaChat.getId());
            MessageHelper.createMessage("Baird's havin a hard time adjustin, you know.",
                    System.currentTimeMillis(), cole, deltaChat.getId());
            MessageHelper.createMessage("Tell Baird to shut up and eat, we'll be back soon.", System.currentTimeMillis(),
                    currentUser, deltaChat.getId());
            MessageHelper.createMessage("Copy that, Cole out.",
                    System.currentTimeMillis(), cole, deltaChat.getId());
        }

        chatListAdapter = new ChatListAdapter(this);
        chatList.setLayoutManager(new LinearLayoutManager(this));
        chatList.setAdapter(chatListAdapter);

        Settings.setListener(this);
        Settings.setUserName("fenix");
    }

    @Override
    protected void onResume() {
        super.onResume();
        chatListAdapter.update();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(getString(R.string.username_key))) {
            loginTextView.setText(Settings.getUserName());
        }
    }
}
