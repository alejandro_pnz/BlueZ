package com.akozhevnikov.bluez.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.akozhevnikov.bluez.R;
import com.akozhevnikov.bluez.model.Chat;
import com.akozhevnikov.bluez.utils.MessageHelper;

import java.util.List;

public class ChatListAdapter extends RecyclerView.Adapter<ChatPreviewHolder> {
    private List<Chat> chats;
    private Context context;
    private SparseArray<ChatPreviewHolder> chatHolders;

    public ChatListAdapter(Context context) {
        this.context = context;
        chatHolders = new SparseArray<>();
        update();
    }

    @Override
    public ChatPreviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_preview_holder, parent, false);
        return new ChatPreviewHolder(context, view);
    }

    @Override
    public void onBindViewHolder(ChatPreviewHolder holder, int position) {
        Chat chat = chats.get(position);
        holder.init(chat);
        if (chatHolders.get(chat.getId()) == null) {
            chatHolders.append(chat.getId(), holder);
        }
    }

    @Override
    public int getItemCount() {
        return chats.size();
    }

    public void update() {
        chats = MessageHelper.getChats();

        for (int i = 0; i < chatHolders.size(); i++) {
            int key = chatHolders.keyAt(i);
            chatHolders.get(key).init(MessageHelper.getChatById(key));
        }
    }

}
