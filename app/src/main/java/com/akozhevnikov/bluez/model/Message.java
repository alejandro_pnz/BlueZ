package com.akozhevnikov.bluez.model;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

public class Message extends RealmObject {
    @Ignore
    public static String ID = "id";
    @Ignore
    public static String CHAT = "chatId";

    @PrimaryKey
    private int id;
    private User sender;
    @Index
    private long createdAt;
    private String text;
    private int chatId;

//    public RealmList<Attachment<String>> attachments;

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getChatId() {
        return chatId;
    }

    public void setChatId(int chatId) {
        this.chatId = chatId;
    }
}

