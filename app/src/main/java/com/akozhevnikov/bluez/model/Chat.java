package com.akozhevnikov.bluez.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class Chat extends RealmObject {
    @Ignore
    public static String NAME = "name";
    @Ignore
    public static String CREATOR = "creator";
    @Ignore
    public static String GUEST = "guest";
    @Ignore
    public static String ID = "id";

    @PrimaryKey
    private int id;
    private String name;
    private User creator;
    private RealmList<User> users;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RealmList<User> getUsers() {
        return users;
    }

    public void setUsers(RealmList<User> users) {
        this.users = users;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
