package com.akozhevnikov.bluez.model;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class User extends RealmObject {
    @Ignore
    public static String FIRST_NAME = "first_name";
    @Ignore
    public static String LAST_NAME = "last_name";
    @Ignore
    public static String PATRONYMIC = "patronymic";
    @Ignore
    public static String USERNAME = "username";

    @PrimaryKey
    private String username;
    @Required
    private String firstName;
    private String lastName;
    private String patronymic;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
