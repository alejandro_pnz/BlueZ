package com.akozhevnikov.bluez.bt;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.util.UUID;

public class ConnectThread extends Thread {
    private final BluetoothSocket clientSocket;
    private final BluetoothDevice clientDevice;

    public ConnectThread(BluetoothDevice device) {
        BluetoothSocket tempSocket = null;
        clientDevice = device;
        try {
            tempSocket = clientDevice.createRfcommSocketToServiceRecord(UUID.randomUUID());
        } catch (IOException e) {
            Log.e(ConnectThread.class.getSimpleName(), e.getMessage());
        }
        clientSocket = tempSocket;
    }

    @Override
    public void run() {
        try {
            clientSocket.connect();
        } catch (IOException connectExc) {
            try {
                Log.e(ConnectThread.class.getSimpleName(), connectExc.getMessage());
                clientSocket.close();
            } catch (IOException closeExc) {
                Log.e(ConnectThread.class.getSimpleName(), closeExc.getMessage());
            }
        }
    }

    public void cancel(){
        try {
            clientSocket.close();
        } catch (IOException e) {
            Log.e(ConnectThread.class.getSimpleName(), e.getMessage());
        }
    }
}
