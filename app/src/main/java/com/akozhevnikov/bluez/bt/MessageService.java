package com.akozhevnikov.bluez.bt;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.akozhevnikov.bluez.R;
import com.akozhevnikov.bluez.model.Chat;
import com.akozhevnikov.bluez.model.User;
import com.akozhevnikov.bluez.utils.MessageHelper;
import com.akozhevnikov.bluez.utils.Settings;

import java.util.List;
import java.util.Set;

public class MessageService extends Service {
    //private NotificationManager notificationManager;
    private BluetoothAdapter bluetoothAdapter;
    private User currentUser;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            Toast.makeText(this, R.string.no_bluetooth, Toast.LENGTH_SHORT).show();
        } else {
//            initCurrentUser();
//            initChats();
        }
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

//    private void initChats() {
//        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
//        if (pairedDevices.size() > 0) {
//            for (BluetoothDevice device : pairedDevices) {
//                User companion = null;
//                List<User> users = MessageHelper.getUsersWhere(User.ADDRESS, device.getAddress());
//
//                if (users.isEmpty()) {
//                    companion = MessageHelper.createUser(device.getName(), device.getAddress());
//                } else {
//                    companion = users.get(0);
//                }
//
//                List<Chat> chats = MessageHelper.getChats();
//                if (!chats.isEmpty()) {
//                    boolean isExist = false;
//                    for (int i = 0; i < chats.size(); i++) {
//                        Chat chat = chats.get(i);
//                        if ((chat.getFirstUserAddress().equals(currentUser.getHardwareAddress()) &&
//                                chat.getSecondUserAddress().equals(companion.getHardwareAddress())) ||
//                                (chat.getFirstUserAddress().equals(companion.getHardwareAddress()) &&
//                                        chat.getSecondUserAddress().equals(currentUser.getHardwareAddress()))) {
//                            isExist = true;
//                        }
//                    }
//                    if (!isExist) {
//                        MessageHelper.createChat(companion.getName(), "",
//                                currentUser.getHardwareAddress(),
//                                companion.getHardwareAddress());
//                    }
//                } else {
//                    MessageHelper.createChat(companion.getName(), "",
//                            currentUser.getHardwareAddress(), companion.getHardwareAddress());
//                }
//            }
//        }
//    }
//
//    private void initCurrentUser() {
//        String currentHardwareAddress = bluetoothAdapter.getAddress();
//        List<User> users =
//                MessageHelper.getUsersWhere(User.ADDRESS, currentHardwareAddress);
//        if (users.isEmpty()) {
//            currentUser = MessageHelper.createUser(bluetoothAdapter.getName(), currentHardwareAddress);
//            Settings.setUserName(getApplicationContext(), bluetoothAdapter.getName());
//            Settings.setHardwareAddress(getApplicationContext(), currentHardwareAddress);
//        } else
//            currentUser = users.get(0);
//    }
}
