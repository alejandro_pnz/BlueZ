package com.akozhevnikov.bluez.bt;

import android.bluetooth.BluetoothSocket;
import android.util.Log;

import com.akozhevnikov.bluez.model.Message;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class CommunicationThread extends Thread {
    private final BluetoothSocket clientSocket;
    private final InputStream inputStream;
    private final OutputStream outputStream;

    public CommunicationThread(BluetoothSocket socket) {
        clientSocket = socket;

        InputStream tmpInput = null;
        OutputStream tmpOutput = null;

        try {
            tmpInput = clientSocket.getInputStream();
            tmpOutput = clientSocket.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

        inputStream = tmpInput;
        outputStream = tmpOutput;
    }

    @Override
    public void run() {
        byte[] buffer = new byte[1024];
        int begin = 0, bytes = 0;

        while (true) {
            try {
                bytes += inputStream.read(buffer, bytes, buffer.length - bytes);
                for (int i = begin; i < bytes; i++) {
                    try {
                        Gson gson = new Gson();
                        gson.fromJson(String.valueOf(bytes), Message.class);
                    } catch (Exception e) {
                        Log.d(CommunicationThread.this.getClass().getSimpleName(), e.getMessage());
                    }
                }
            } catch (IOException e) {
                Log.d(CommunicationThread.this.getClass().getSimpleName(), e.getMessage());
            }
        }
    }
}
