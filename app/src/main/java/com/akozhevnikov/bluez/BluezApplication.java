package com.akozhevnikov.bluez;

import android.app.Application;

import com.akozhevnikov.bluez.utils.Settings;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class BluezApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        RealmConfiguration config = new RealmConfiguration.Builder(this)
                .name("default")
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
        Settings.CONTEXT = getApplicationContext();
    }
}
