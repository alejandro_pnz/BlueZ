package com.akozhevnikov.bluez.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.akozhevnikov.bluez.R;

public class Settings {
    public static Context CONTEXT;

    private Settings(){}

    public static void setUserName(String name){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(CONTEXT);
        preferences.edit().putString(CONTEXT.getString(R.string.username_key), name).apply();
    }

    public static String getUserName(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(CONTEXT);
        return preferences.getString( CONTEXT.getString(R.string.username_key), "");
    }

    public static void setListener(SharedPreferences.OnSharedPreferenceChangeListener l){
        PreferenceManager.getDefaultSharedPreferences(CONTEXT)
                .registerOnSharedPreferenceChangeListener(l);
    }
}
