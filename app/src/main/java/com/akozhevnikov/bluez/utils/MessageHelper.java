package com.akozhevnikov.bluez.utils;

import android.text.TextUtils;

import com.akozhevnikov.bluez.model.Chat;
import com.akozhevnikov.bluez.model.Message;
import com.akozhevnikov.bluez.model.User;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

public class MessageHelper {

    public static List<Chat> getChatsWhere(String field, String value) {
        return Realm.getDefaultInstance()
                .where(Chat.class)
                .equalTo(field, value)
                .findAll();
    }

    public static Chat getChatById(int id) {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Chat> results = realm
                .where(Chat.class)
                .equalTo(Chat.ID, id)
                .findAll();
        if (results.size() != 0)
            return results.get(0);
        else
            return null;
    }

    public static List<User> getUsersWhere(String field, String value) {
        return Realm.getDefaultInstance()
                .where(User.class)
                .equalTo(field, value)
                .findAll();
    }

    public static List<User> getUsers() {
        return Realm.getDefaultInstance()
                .where(User.class)
                .findAll();
    }

    public static synchronized List<Chat> getChats() {
        return Realm.getDefaultInstance()
                .where(Chat.class)
                .findAll();
    }

    public static User createUser(String username, String firstName, String lastName,
                                  String patronymic) {
        if (username == null && firstName == null) {
            return null;
        }
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        User user = realm.createObject(User.class);
        user.setUsername(username);
        user.setFirstName(firstName);
        if (!TextUtils.isEmpty(lastName)) {
            user.setLastName(lastName);
        }
        if (!TextUtils.isEmpty(patronymic)) {
            user.setPatronymic(patronymic);
        }
        realm.commitTransaction();
        return user;
    }

    public static Message createMessage(String messageText, long createdAt, User sender, int chatId) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Message message = realm.createObject(Message.class);
        message.setId(getMessageNextKey());
        message.setCreatedAt(createdAt);
        message.setText(messageText);
        message.setSender(sender);
        message.setChatId(chatId);
        realm.commitTransaction();
        return message;
    }

    public static Chat createChat(RealmList<User> users, User creator, String chatName) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Chat chat = realm.createObject(Chat.class);
        chat.setId(getChatNextKey());
        if (users.size() > 2) {
            chat.setName(chatName);
            chat.setCreator(creator);
        } else if (users.size() < 2) {
            return null;
        }

        chat.setUsers(users);
        realm.commitTransaction();
        return chat;
    }

    public static List<Message> getMessagesByChat(Chat chat) {
        return Realm.getDefaultInstance()
                .where(Message.class)
                .equalTo(Message.CHAT, chat.getId())
                .findAll();
    }

    private static List<Message> getMessages() {
        return Realm.getDefaultInstance()
                .where(Message.class)
                .findAll();
    }

    public static int getMessageNextKey() {
        if (getMessages().size() > 0)
            return Realm.getDefaultInstance().where(Message.class).max(Message.ID).intValue() + 1;
        else
            return 1;
    }

    public static int getChatNextKey() {
        if (getChats().size() > 0)
            return Realm.getDefaultInstance().where(Chat.class).max(Chat.ID).intValue() + 1;
        else
            return 1;
    }
}
